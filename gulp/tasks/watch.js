/**
 * Created by thinkpad on 22.03.17.
 */
'use strict';

var config = require('../config');
var gulp = require('gulp');
var livereload = require('gulp-livereload');

gulp.task('watch', function() {

    gulp.watch(config.path.app_sass + '*.scss', ['sass']);

});
