/**
 * Created by thinkpad on 20.03.17.
 */
'use strict';

var config = require('../config');
var gulp = require('gulp');
var sass = require('gulp-sass');
// var concatCss = require('gulp-concat-css');

gulp.task('sass', ['clean-sass'], function() {
    var task = null;

    // gulp.src(config.path.bowerComponents + '/bootstrap/dist/css/bootstrap.min.css')
    //     .pipe(gulp.dest(config.path.css));

    gulp.src(config.path.app_sass + 'backend.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(config.path.app_css))
        .pipe(gulp.dest(config.path.web_css));

    return task;
});


// gulp.task('default', function () {
//     return gulp.src('assets/**/*.css')
//         .pipe(concatCss("styles/bundle.css"))
//         .pipe(gulp.dest('out/'));
// });


// gulp.task('sass', function () {
//     return gulp.src('./sass/**/*.scss')
//         .pipe(sass().on('error', sass.logError))
//         .pipe(gulp.dest('./css'));
// });
