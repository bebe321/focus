/**
 * Created by thinkpad on 20.03.17.
 */
'use strict';

var gulp = require('gulp');

gulp.task('default', [
    'clean',
    'sass',
    'js'
]);
