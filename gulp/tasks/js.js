/**
 * Created by thinkpad on 20.03.17.
 */
'use strict';

var config = require('../config');
var gulp = require('gulp');

gulp.task('js', ['clean-js'], function() {
    var task = null;

    gulp.src(config.path.bowerComponents + '/jquery/dist/jquery.slim.min.js')
        .pipe(gulp.dest(config.path.web_js));

    gulp.src(config.path.bowerComponents + '/tether/dist/js/tether.min.js')
        .pipe(gulp.dest(config.path.web_js));

    gulp.src(config.path.bowerComponents + '/bootstrap/dist/js/bootstrap.min.js')
        .pipe(gulp.dest(config.path.web_js));

    return task;
});
