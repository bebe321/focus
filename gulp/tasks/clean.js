/**
 * Created by thinkpad on 20.03.17.
 */
'use strict';

var config = require('../config');
var gulp = require('gulp');
var del = require('del');

gulp.task('clean', [
    'clean-sass',
    'clean-js'
]);

gulp.task('clean-sass', function() {

    del(config.path.app_css);

    return del(config.path.web_css);
});

gulp.task('clean-js', function() {
    return del(config.path.web_js);
});
