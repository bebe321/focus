/**
 * Created by thinkpad on 20.03.17.
 */

/*
 * The "path" part of configuration.
 * Contains paths of important directories.
 */
var path = {
    bowerComponents: './bower_components',
    app_sass: './app/Resources/sass/',
    app_css: './app/Resources/css/',
    web_css: './web/css',
    web_js: './web/js'
};

/*
 * Finally, all parameters of configuration
 */
module.exports = {
    path: path
};


// app/Resources/css
// app_css: './src/LayoutBundle/Resources/css/',
// app_sass: './src/LayoutBundle/Resources/sass/',
