<?php

namespace CommonBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

/**
 * CrudEvent
 * Created on 2017-03-23
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class CrudEvent extends Event
{
    const CREATE = 'crud.event.create';

    const READ = 'crud.event.read';

    const UPDATE = 'crud.event.update';

    const DELETE = 'crud.event.delete';

    /**
     * The request
     *
     * @var Request
     */
    private $request;

    /**
     * The entity
     *
     * @var Request
     */
    private $entity;

    public function __construct(Request $request, $entity)
    {
        $this->request = $request;
        $this->entity = $entity;

        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return object
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param object $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }
}
