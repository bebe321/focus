<?php

namespace CommonBundle\Controller;

use CommonBundle\Event\CrudEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * AbstractController
 * Created on 2017-03-23
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
abstract class AbstractController extends Controller
{
    /**
     * @var string
     */
    private $entityClassName;

    /**
     * @return Response
     */
    public function indexAction()
    {
        $entities = $this->getDoctrine()
            ->getRepository($this->getEntityClassName())
            ->findAll();

        return $this->render($this->getIndexViewPath(), ['entities' => $entities]);
    }

    /**
     * todo: try custom ParamConverter or something different
     *
     * @param Request $request
     * @param         $id
     * @return string
     */
    public function readAction(Request $request, $id)
    {
        $entity = $this->getDoctrine()
            ->getRepository($this->getEntityClassName())
            ->find($id);

        $event = new CrudEvent($request, $entity);
        $this->get('event_dispatcher')->dispatch(CrudEvent::READ, $event);

        return $this->render($this->getReadViewPath(), ['entity' => $entity]);
    }

    /**
     * @param Request $request
     * @param         $id
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $entity = $this->getDoctrine()
            ->getRepository($this->getEntityClassName())
            ->find($id);

        $event = new CrudEvent($request, $entity);
        $this->get('event_dispatcher')->dispatch(CrudEvent::DELETE, $event);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($entity);
        $entityManager->flush();

        return $this->redirectToRoute($this->getIndexRoute());
    }

    /**
     * @param Request $request
     * @param         $id
     * @return Response
     */
    public function updateAction(Request $request, $id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $entity = $this->getDoctrine()
            ->getRepository($this->getEntityClassName())
            ->find($id);

        $form = $this->createForm($this->getEntityFormType(), $entity)
            ->add('Save', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $event = new CrudEvent($request, $entity);
            $this->get('event_dispatcher')->dispatch(CrudEvent::UPDATE, $event);

            $entityManager->flush();

            return $this->redirectToRoute($this->getReadRoute(), ['id' => $entity->getId()]);
        }

        return $this->render($this->getUpdateViewPath(), [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        /** @var object $entity */
        $entity = $this->getEntityObject();

        $form = $this->createForm($this->getEntityFormType(), $entity)
            ->add('Save', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $event = new CrudEvent($request, $entity);
            $this->get('event_dispatcher')->dispatch(CrudEvent::CREATE, $event);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            if ($form->get('Save')->isClicked()) {
                return $this->redirectToRoute($this->getReadRoute(), ['id' => $entity->getId()]);
            }

            return $this->redirectToRoute($this->getIndexRoute());
        }

        return $this->render($this->getUpdateViewPath(), [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * @return string
     */
    public function getEntityClassName()
    {
        if ($this->entityClassName == null) {
            $entity = $this->getEntityObject();
            $fullEntityClassName = get_class($entity);
            $array = explode('\\', $fullEntityClassName);

            $bundleName = current($array);
            $shortEntityClassName = end($array);

            $this->entityClassName = sprintf('%s:%s', $bundleName, $shortEntityClassName);
        }

        return $this->entityClassName;
    }

    abstract public function getEntityObject();

    abstract protected function getEntityFormType();

    abstract protected function getIndexViewPath();

    abstract protected function getReadViewPath();

    abstract protected function getUpdateViewPath();

    abstract protected function getIndexRoute();

    abstract protected function getReadRoute();

}
