<?php

namespace CommonBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * DoctrineSubscriber
 * Created on 2017-03-17
 *
 * Lists of Doctrine events
 * #http://doctrine-orm.readthedocs.io/en/latest/reference/events.html#entity-listeners
 *
 * CREATE - Persist
[2017-03-29 23:35:19] focus.INFO: DoctrineSubscriber: loadClassMetadata id:CommonBundle\Entity\AbstractEntity [] []
[2017-03-29 23:35:19] focus.INFO: DoctrineSubscriber: loadClassMetadata id:AppBundle\Entity\Post [] []
 * form
[2017-03-29 23:35:35] focus.INFO: DoctrineSubscriber: loadClassMetadata id:CommonBundle\Entity\AbstractEntity [] []
[2017-03-29 23:35:35] focus.INFO: DoctrineSubscriber: loadClassMetadata id:AppBundle\Entity\Post [] []
[2017-03-29 23:35:35] focus.INFO: DoctrineSubscriber: prePersist [] []
[2017-03-29 23:35:35] focus.INFO: DoctrineSubscriber: preFlush [] []
[2017-03-29 23:35:35] focus.INFO: DoctrineSubscriber: onFlush [] []
[2017-03-29 23:35:35] focus.INFO: DoctrineSubscriber: postPersist [] []
[2017-03-29 23:35:35] focus.INFO: DoctrineSubscriber: postFlush [] []
 *
 *
 * READ
[2017-03-29 23:31:45] focus.INFO: DoctrineSubscriber: loadClassMetadata id:CommonBundle\Entity\AbstractEntity [] []
[2017-03-29 23:31:45] focus.INFO: DoctrineSubscriber: loadClassMetadata id:AppBundle\Entity\Post [] []
[2017-03-29 23:31:45] focus.INFO: DoctrineSubscriber: postLoad id:19 [] []
 *
 *
 *
 * UPDATE
[2017-03-29 23:39:14] focus.INFO: DoctrineSubscriber: loadClassMetadata id:CommonBundle\Entity\AbstractEntity [] []
[2017-03-29 23:39:14] focus.INFO: DoctrineSubscriber: loadClassMetadata id:AppBundle\Entity\Post [] []
[2017-03-29 23:39:14] focus.INFO: DoctrineSubscriber: postLoad id:19 [] []
 * form
[2017-03-29 23:39:20] focus.INFO: DoctrineSubscriber: loadClassMetadata id:CommonBundle\Entity\AbstractEntity [] []
[2017-03-29 23:39:20] focus.INFO: DoctrineSubscriber: loadClassMetadata id:AppBundle\Entity\Post [] []
[2017-03-29 23:39:20] focus.INFO: DoctrineSubscriber: postLoad id:19 [] []
[2017-03-29 23:39:20] focus.INFO: DoctrineSubscriber: preFlush [] []
[2017-03-29 23:39:20] focus.INFO: DoctrineSubscriber: onFlush [] []
[2017-03-29 23:39:20] focus.INFO: DoctrineSubscriber: preUpdate [] []
[2017-03-29 23:39:20] focus.INFO: DoctrineSubscriber: postUpdate [] []
[2017-03-29 23:39:20] focus.INFO: DoctrineSubscriber: postFlush [] []
 *
 *
 * DELETE - Remove
[2017-03-29 23:30:09] focus.INFO: DoctrineSubscriber: postLoad id:17 [] []
[2017-03-29 23:30:09] focus.INFO: DoctrineSubscriber: preRemove [] []
[2017-03-29 23:30:09] focus.INFO: DoctrineSubscriber: preFlush [] []
[2017-03-29 23:30:09] focus.INFO: DoctrineSubscriber: onFlush [] []
[2017-03-29 23:30:09] focus.INFO: DoctrineSubscriber: postRemove [] []
[2017-03-29 23:30:09] focus.INFO: DoctrineSubscriber: postFlush [] []
 *
 *
 *
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class DoctrineSubscriber implements EventSubscriber
{
    private $session;

    private $logger;

    public function __construct(Session $session, Logger $logger)
    {
        $this->session = $session;
        $this->logger = $logger;
    }

    public function getSubscribedEvents()
    {
        return [
            'preRemove',
            'postRemove',
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
            'postLoad',
            'loadClassMetadata',
            'onClassMetadataNotFound',
            'preFlush',
            'onFlush',
            'postFlush',
            'onClear',
        ];
    }

    /**
     * loadClassMetadata - The loadClassMetadata event occurs after the mapping metadata for a class
     * has been loaded from a mapping source (annotations/xml/yaml). This event is not a lifecycle callback.
     *
     * @param LoadClassMetadataEventArgs $args
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $args)
    {
        $classMetadata = $args->getClassMetadata();
        $this->log(sprintf('loadClassMetadata id:%s', $classMetadata->getMetadataValue('name')  ));
    }

    public function onClassMetadataNotFound()
    {
        $this->log('onClassMetadataNotFound');
    }

    public function onClear()
    {
        $this->log('onClear');
    }

    public function onFlush()
    {
        $this->log('onFlush');
    }

    public function postFlush()
    {
        $this->log('postFlush');
    }

    /**
     * postLoad - The postLoad event occurs for an entity after the entity has been loaded into
     * the current EntityManager from the database or after the refresh operation has been applied to it.
     *
     * @param LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->log(sprintf('postLoad id:%s', $entity->getId()  ));
    }

    public function postPersist()
    {
        $this->log('postPersist');
    }

    public function postRemove()
    {
        $this->log('postRemove');
    }

    public function postUpdate()
    {
        $this->log('postUpdate');
    }

    public function preFlush()
    {
        $this->log('preFlush');
    }

    public function prePersist()
    {
        $this->log('prePersist');
    }

    public function preRemove()
    {
        $this->log('preRemove');
    }

    public function preUpdate()
    {
        $this->log('preUpdate');
    }

    private function log($message)
    {
        $this->logger->info(sprintf('DoctrineSubscriber: %s', $message));
    }


}
