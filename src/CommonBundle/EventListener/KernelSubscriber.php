<?php

namespace CommonBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelSubscriber implements EventSubscriberInterface
{

    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST        => 'requestHandler',
            KernelEvents::CONTROLLER     => 'controllerHandler',
            KernelEvents::VIEW           => 'viewHandler',
            KernelEvents::RESPONSE       => 'responseHandler',
            KernelEvents::FINISH_REQUEST => 'finishHandler',
            KernelEvents::TERMINATE      => 'terminateHandler',
            KernelEvents::EXCEPTION      => 'exceptionHandler',
        ];
    }


    public function requestHandler(GetResponseEvent $event)
    {

        $parameters = $event->getRequest()->attributes;
        $name = $event->getRequest()->get('_route');

        $parameters->set('param', 'test param');

        $this->session->getFlashBag()->add('notice', sprintf('KernelEvents::REQUEST %s', $name));
    }

    public function controllerHandler(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        $controllerName = $event->getRequest()->get('_controller');

        $result = is_callable($controller);

        $this->session->getFlashBag()->add('notice', sprintf('KernelEvents::CONTROLLER %s %s', $result, $controllerName));
    }

    public function viewHandler(GetResponseForControllerResultEvent $event)
    {
        $this->session->getFlashBag()->add('notice', 'KernelEvents::VIEW');

        $result = $event->getControllerResult();
        $response = new Response($result . ' KernelEvents::VIEW');

        $event->setResponse($response);

    }

    public function responseHandler(FilterResponseEvent $event)
    {
        $statusCode = $event->getResponse()->getStatusCode();
        $date = $event->getResponse()->getDate();

        $this->session->getFlashBag()->add('notice', sprintf('KernelEvents::RESPONSE %s %s', $statusCode, $date->format('d.m.y h:m:s')));

        $response = $event->getResponse();
        $content = $response->getContent();

        $response->setContent($content .'<h4>new content response</h4>');

    }

    public function finishHandler(FinishRequestEvent $event)
    {
        $this->session->getFlashBag()->add('notice', 'KernelEvents::FINISH_REQUEST ' . $event->getRequestType());
    }

    public function terminateHandler(PostResponseEvent $event)
    {
        $this->session->getFlashBag()->add('notice', 'KernelEvents::TERMINATE' . $event->getRequestType());
    }

    public function exceptionHandler(GetResponseForExceptionEvent $event)
    {

        // you can alternatively set a new Exception
//         $exception = new \Exception('Some special exception');
//         $event->setException($exception);

        $this->session->getFlashBag()->add('notice', 'KernelEvents::EXCEPTION' . $event->getRequestType());
    }

}
