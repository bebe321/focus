<?php

namespace CommonBundle\EventListener;

use AppBundle\Entity\Post;
use CommonBundle\Event\CrudEvent;
use CommonBundle\Helper\CrudHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * CrudSubscriber
 * Created on 2017-03-23
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class CrudSubscriber implements EventSubscriberInterface
{
    private $session;

    private $crudHelper;

    public function __construct(Session $session, CrudHelper $crudHelper)
    {
        $this->crudHelper = $crudHelper;
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
            CrudEvent::CREATE => 'onCreate',
            CrudEvent::READ   => 'onRead',
            CrudEvent::UPDATE => 'onUpdate',
            CrudEvent::DELETE => 'onDelete',
        ];
    }

    public function onCreate(CrudEvent $crudEvent)
    {
        $entity = $crudEvent->getEntity();

        if ($entity instanceof Post && $entity !== null) {
            $entity->setSlug($this->crudHelper->generateSlug($entity->getName()));
            $entity->setCreatedAt(new \DateTime());

            $this->session->getFlashBag()->add('success', sprintf('Create new the Post: %s', $entity->getName()));
        }
    }

    public function onRead(CrudEvent $crudEvent)
    {
        $entity = $crudEvent->getEntity();

        if ($entity instanceof Post && $entity !== null) {
            $date = new \DateTime();

            $this->session->getFlashBag()->add('success', sprintf('You are reading this Post about %s',
                $date->format('Y-m-d H:i:s')));
        }
    }

    public function onUpdate(CrudEvent $crudEvent)
    {
        $entity = $crudEvent->getEntity();

        if ($entity instanceof Post && $entity !== null) {
            $entity->setSlug($this->crudHelper->generateSlug($entity->getName()));

            $this->session->getFlashBag()->add('success', sprintf('Update slug on the Post: %s', $entity->getSlug()));
        }
    }

    public function onDelete(CrudEvent $crudEvent)
    {
        $entity = $crudEvent->getEntity();

        if ($entity instanceof Post && $entity !== null) {

            $this->session->getFlashBag()->add('success', sprintf('Delete the Post: %s', $entity->getName()));
        }
    }
}
