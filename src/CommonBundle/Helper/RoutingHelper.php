<?php

namespace CommonBundle\Helper;

use Symfony\Component\Routing\Router;

/**
 * RoutingHelper
 * Created on 2017-03-21
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class RoutingHelper
{
    /**
     * The Router service used serve routes, urls etc.
     *
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function generateUrl($routeName, $routeParameters = [])
    {
        $url = $this->router->generate($routeName, $routeParameters);

        return $url;
    }
}
