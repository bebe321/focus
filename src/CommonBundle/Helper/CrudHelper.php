<?php

namespace CommonBundle\Helper;

/**
 * SlugHelper
 * Created on 2017-03-23
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class CrudHelper
{
    /**
     * @param string $string
     * @return string
     */
    public function generateSlug($string)
    {
        return preg_replace('/\s+/', '-', mb_strtolower(trim(strip_tags($string)), 'UTF-8'));
    }
}
