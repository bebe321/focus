<?php

namespace CommonBundle\Twig;

use CommonBundle\Helper\RoutingHelper;
use Twig_SimpleFunction;

/**
 * FunctionsExtension
 * Created on 2017-03-21
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class FunctionsExtension extends \Twig_Extension
{
    private $routingHelper;

    public function __construct(RoutingHelper $routingHelper)
    {
        $this->routingHelper = $routingHelper;
    }

    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
        ];

        $callable = [
            1 => [
                $this,
                'getCreateButton',
            ],
            2 => [
                $this,
                'getReadButton',
            ],
            3 => [
                $this,
                'getUpdateButton',
            ],
            4 => [
                $this,
                'getDeleteButton',
            ],
        ];

        $functions = [
            new Twig_SimpleFunction('focus_create_button', $callable[1], $options),
            new Twig_SimpleFunction('focus_read_button', $callable[2], $options),
            new Twig_SimpleFunction('focus_update_button', $callable[3], $options),
            new Twig_SimpleFunction('focus_delete_button', $callable[4], $options),
        ];

        return array_merge($functions, parent::getFunctions());
    }

    public function getCreateButton($routeName)
    {
        $url = $this->routingHelper->generateUrl($routeName);
        $createBtn = "<a href=" . "%s" . " class='btn btn-primary'>Create</a>";

        return sprintf($createBtn, $url);
    }

    public function getReadButton($id, $title, $routeName)
    {
        $url = $this->routingHelper->generateUrl($routeName, ['id' => $id]);
        $readBtn = "<a href=" . "%s" . ">%s</a>";

        return sprintf($readBtn, $url, $title);
    }

    public function getUpdateButton($id, $routeName)
    {
        $url = $this->routingHelper->generateUrl($routeName, ['id' => $id]);
        $updateBtn = "<a href=" . "%s" . " class='btn btn-warning btn-sm'>Update</a>";

        return sprintf($updateBtn, $url);
    }

    public function getDeleteButton($id, $routeName)
    {
        $url = $this->routingHelper->generateUrl($routeName, ['id' => $id]);
        $deleteBtn = "<a href=" . "%s" . " class='btn btn-danger btn-sm'>Delete</a>";

        return sprintf($deleteBtn, $url);
    }

}
