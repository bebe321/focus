<?php

namespace CommonBundle\Twig;

use DateTime;
use Twig_SimpleFilter;

/**
 * FiltersExtension
 * Created on 2017-03-21
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class FiltersExtension extends \Twig_Extension
{
    public function getFilters()
    {
        $callable = [
            1 => [
                $this,
                'getMaxCharacters',
            ],
            2 => [
                $this,
                'getDatetime',
            ],
        ];

        $filters = [
            new Twig_SimpleFilter('focus_max_characters', $callable[1]),
            new Twig_SimpleFilter('focus_datetime', $callable[2]),
        ];

        return array_merge($filters, parent::getFilters());
    }

    public function getMaxCharacters($string, $max = 15)
    {
        $format = '%s...';

        if ($max > strlen($string)) {
            $max = strlen($string);
            $format = '%s';
        }
        $string = substr($string, 0, $max);

        return sprintf($format, $string);
    }

    public function getDatetime(DateTime $dateTime)
    {
        return $dateTime->format('Y-m-d H:i:s');
    }

}
