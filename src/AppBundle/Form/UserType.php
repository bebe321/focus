<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\User;

/**
 * UserType
 * Created on 2017-03-25
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('firstName', null, [
                'label' => 'First Name',
            ])
            ->add('lastName', null, [
                'label' => 'Last Name',
            ])
            ->add('userName', null, [
                'label' => 'User Name',
            ])
            ->add('email', null, [
                'label' => 'E-mail',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
