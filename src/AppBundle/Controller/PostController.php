<?php

namespace AppBundle\Controller;

use CommonBundle\Controller\AbstractController;
use AppBundle\Entity\Post;
use AppBundle\Form\PostType;

/**
 * PostController
 * Created on 2017-03-23
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class PostController extends AbstractController
{
    public function getEntityObject()
    {
        return new Post();
    }

    protected function getEntityFormType()
    {
        return PostType::class;
    }

    protected function getIndexViewPath()
    {
        return 'Post/index.html.twig';
    }

    protected function getReadViewPath()
    {
        return 'Post/read.html.twig';
    }

    protected function getUpdateViewPath()
    {
        return 'Post/update.html.twig';
    }

    protected function getIndexRoute()
    {
        return 'post.index';
    }

    protected function getReadRoute()
    {
        return 'post.read';
    }
}
