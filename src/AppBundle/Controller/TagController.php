<?php

namespace AppBundle\Controller;

use CommonBundle\Controller\AbstractController;
use AppBundle\Entity\Tag;
use AppBundle\Form\TagType;

/**
 * TagController
 * Created on 2017-03-26
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class TagController extends AbstractController
{

    public function getEntityObject()
    {
        return new Tag();
    }

    protected function getEntityFormType()
    {
        return TagType::class;
    }

    protected function getIndexViewPath()
    {
        return '/Tag/index.html.twig';
    }

    protected function getReadViewPath()
    {
        return '/Tag/read.html.twig';
    }

    protected function getUpdateViewPath()
    {
        return '/Tag/update.html.twig';
    }

    protected function getIndexRoute()
    {
        return 'tag.index';
    }

    protected function getReadRoute()
    {
        return 'tag.read';
    }
}
