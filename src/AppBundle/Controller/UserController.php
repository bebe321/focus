<?php

namespace AppBundle\Controller;

use CommonBundle\Controller\AbstractController;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;

/**
 * UserController
 * Created on 2017-03-24
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class UserController extends AbstractController
{

    public function getEntityObject()
    {
        return new User();
    }

    protected function getEntityFormType()
    {
        return UserType::class;
    }

    protected function getIndexViewPath()
    {
        return '/User/index.html.twig';
    }

    protected function getReadViewPath()
    {
        return '/User/read.html.twig';
    }

    protected function getUpdateViewPath()
    {
        return '/User/update.html.twig';
    }

    protected function getIndexRoute()
    {
        return 'user.index';
    }

    protected function getReadRoute()
    {
        return 'user.read';
    }
}
