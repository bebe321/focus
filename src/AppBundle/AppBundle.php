<?php

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * AppBundle
 * Created on 2017-03-29
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class AppBundle extends Bundle
{

}
