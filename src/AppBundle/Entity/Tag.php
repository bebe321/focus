<?php

namespace AppBundle\Entity;

use CommonBundle\Entity\AbstractEntity;
use CommonBundle\Entity\SimpleTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tag
 * Created on 2017-03-25
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TagRepository")
 * @ORM\Table(name="tags")
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class Tag extends AbstractEntity
{
    use SimpleTrait;

}
