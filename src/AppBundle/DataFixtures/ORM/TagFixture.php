<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use AppBundle\Entity\Tag;

/**
 * TagFixture
 * Created on 2017-03-25
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class TagFixture implements FixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 1; $i <= 50; $i++) {
            $tag = new Tag();
            $tag->setName($faker->word);
            $tag->setSlug($tag->getName());

            $tag->setCreatedAt($faker->dateTimeThisMonth());

            $manager->persist($tag);
        }

        $manager->flush();
    }
}
