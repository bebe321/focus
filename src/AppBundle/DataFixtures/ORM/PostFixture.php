<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Post;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

/**
 * PostFixture
 * Created on 2017-03-17
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class PostFixture implements FixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 1; $i <= 19; $i++) {
            $post = new Post();
            $post->setName($faker->sentence(3));
            $post->setSlug('slug' . $i);
            $post->setSummary($faker->paragraph(2));
            $post->setContent($faker->paragraph(24)); //todo: tests paragraphs

            $post->setCreatedAt($faker->dateTimeThisMonth());

            $manager->persist($post);
        }

        $manager->flush();
    }
}
