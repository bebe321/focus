<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use AppBundle\Entity\User;

/**
 * UserFixture
 * Created on 2017-03-24
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class UserFixture implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 1; $i <= 5; $i++) {
            $user = new User();
            $user->setUserName($faker->userName);
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setEmail($faker->email);
            $user->setCreatedAt($faker->dateTimeThisMonth());

            $manager->persist($user);
        }

        $manager->flush();
    }
}
