<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * PostRepository
 * Created on 2017-03-17
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class PostRepository extends EntityRepository
{
}
