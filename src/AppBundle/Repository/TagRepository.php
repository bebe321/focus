<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * TagRepository
 * Created on 2017-03-25
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class TagRepository extends EntityRepository
{
}
