<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 * Created on 2017-03-24
 *
 * @author Bogumił Brzeziński <bogumilbr@gmail.com>
 */
class UserRepository extends EntityRepository
{
}
